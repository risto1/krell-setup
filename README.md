# Krell setup

## How To Install Android Studio on Void Linux

https://docs.voidlinux.org/xbps/repositories/restricted.html

0. Install non-free
sudo xbps-install -S void-repo-nonfree

1. Clone packages
https://github.com/void-linux/void-packages

2. Bootstrap it
./xbps-src binary-bootstrap

3. Allow restricted
echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf

4. Build (extract) android-studio
./xbps-src pkg android-studio

5. Install it
sudo xbps-install --repository hostdir/binpkgs/nonfree android-studio


## Set up SDK

Review setup instructions for React Native CLI (click tab) here:
https://reactnative.dev/docs/environment-setup

- Download the SDK
- In splash screen, go to Configure > AVD Manager to configure/add a device for the emulator
- Disable hardware acceleration if necessary (can crash some systems)



## Setup Flow

1. Install node deps:

``` sh
yarn install
```

2. Install the REPL support dependencies:

``` sh
clj -m cljs.main --install-deps
```

3. Build project:

``` sh
clj -m krell.main -v -co build.edn -c
```

4. Cider jack in krell

```
C-c C-x j s
```

Select `krell` and `:app`

5. Start react native's metro bundler:

``` sh
npm start
```

6. Start android emulator:

``` sh
npm run android
```

NOTE: On first load it messes up and says it can't find `goog.require` in the emaultor and metro
logs:

```
ERROR    goog.require could not find: awesome_project.core
ERROR    Error: goog.require could not find: awesome_project.core
```

Just restart the bundle in the metro bundler by pressing `r`, and it should show up ok after that
